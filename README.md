# terraform-test

# Part-1 : Açıklama ve Örnekler

- Bu örnekte `gitlab-terraform` kullanılacak (Infrastructure as Code with Terraform and GitLab). Terraform `state` file tamamen gitlab tarafından manage edilmektedir. İstersek gitlab dashboard üzerinden state file'ı lokalimize `migrate` edebiliriz ve lokalden yönetebiliriz, ama bu örnekte bu işlemi yapmayacağız. Bir sonraki örnekte anlatılacaktır.
  GitLab tarafından job'ların içerisinde koşmasında kullanılan runner için temel alınan `image` bize gitlab-terraform yani terraform komutlarını kullanma imkanı veriyor. Bu sayede infrastructure oluşturabiliyoruz ama resource'ların oluşacağı provider'ın `credentials` bilgilerini `sensitive variable` olarak gitlab'a tanıtmalıyız.
  - [https://docs.gitlab.com/ee/user/infrastructure/iac/]
  - [https://docs.gitlab.com/ee/user/infrastructure/iac/gitlab_terraform_helpers.html]

- Burada örnek olması açısından farklı`keywords` kullanılmıştır. `hidden job , extends , needs, include, template` kullanımını görebiliriz.

- `hidden job` : hazırladığın bir job'u bir başka job içerisinde referans göstererek kullanabilirsin. [https://docs.gitlab.com/ee/ci/jobs/#hide-jobs]

- `extends` : referans göstereceğin `hidden job`u belirtmek için kullanılır. [https://docs.gitlab.com/ee/ci/yaml/index.html#extends]
  - Aynı pipeline içerisinde öncesinde hidden job olarak hazırlanmış job'u başka bir job içinde referans gösterebilirsin.
  - Önceden hazırlanmış `.gitlab-ci.yml` dosyasını `include` ifadesiyle `template` olarak kullandığında ve pipeline başında bunu belirttiğinde, o `template` içindeki hidden-job'ları referans gösterirken kullanabilirsin. Kullanılan template sizin lokalinizde de olabilir, başka uzak kaynaklarda da olabilir.

```yaml (hidden job - extends example)
.hidden_job:
  script: 
    - ls

extends_job:
  extends: .hidden_job
```

- `include` and `template`
  - [https://docs.gitlab.com/ee/ci/yaml/index.html#includetemplate]
  - [https://docs.gitlab.com/ee/ci/yaml/index.html#include]
  - [https://docs.gitlab.com/ee/user/infrastructure/iac/#use-a-terraform-template]
```yaml (include and template example)  
include:
  - template: Terraform/Base.gitlab-ci.yml
```

- `needs` bulunduğu job öncesinde yapılmasını istediğiniz bir job varsa belirtmek için veya diğer joblardan bağımsız olarak bulunduğunuz job'un tamamlanması için kullanırsınız.  [https://docs.gitlab.com/ee/ci/yaml/index.html#needs]

```yaml (needs example)
linux:build:
  stage: build
  script: echo "Building linux..."

mac:build:
  stage: build
  script: echo "Building mac..."

lint:
  stage: test
  needs: []
  script: echo "Linting..."

linux:rspec:
  stage: test
  needs: ["linux:build"]
  script: echo "Running rspec on linux..."

mac:rspec:
  stage: test
  needs: ["mac:build"]
  script: echo "Running rspec on mac..."

production:
  stage: deploy
  script: echo "Running production..."
  environment: production

# This example creates four paths of execution:

# Linter: The lint job runs immediately without waiting for the build stage to complete because it has no needs (needs: []).
# Linux path: The linux:rspec job runs as soon as the linux:build job finishes, without waiting for mac:build to finish.
# macOS path: The mac:rspec jobs runs as soon as the mac:build job finishes, without waiting for linux:build to finish.
# The production job runs as soon as all previous jobs finish: linux:build, linux:rspec, mac:build, mac:rspec.
```

# Part-2: `main.tf` & `Backend.tf`

- `main.tf`
```go
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "terraform-bucket" {
  bucket = "arrow.terraform.gitlab"
}
```

- `Backend.tf` GitLab tarafından managed olacak state file için backend
  - [https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html]
  - [https://developer.hashicorp.com/terraform/language/settings/backends/http]

```go (Backend.tf)
terraform {
  backend "http" {
  }
}
```



# Part-3: `gitlab-ci.yml` Aşamalı olarak hazırlanıp denenecektir.

- 1. gitlab-ci.yml `Version_1.0`

```yaml (gitlab-ci.yml)
stages:
  - format
  - validate

image:
  name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:v1.0.0"

variables:
  TF_ROOT: ${CI_PROJECT_DIR} 
  TF_STATE_NAME: default  # The name of the state file used by the GitLab Managed Terraform state backend
  # TF_STATE_NAME : If TF_ADDRESS is not set, and TF_STATE_NAME is provided, then the value of TF_STATE_NAME is used as GitLab-managed Terraform State name.

.terraform:format:
  stage: format
  script:
    - whoami
    - pwd
    - ls
    - gitlab-terraform fmt
  allow_failure: true

.terraform:validate:
  stage: validate
  script:
    - gitlab-terraform validate
  allow_failure: true

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

terraform:format:
  stage: format
  extends: .terraform:format
  needs: []

terraform:validate:
  stage: validate
  extends: .terraform:validate
  needs: []
```
  - Commit changes  --> Pipelines     ===>>> pipeline ve job'ları gözlemle


- 2. gitlab-ci.yml `Version_1.1`

```yaml (gitlab-ci.yml)
stages:
  - format
  - validate

image:
  name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:v1.0.0"

variables:
  TF_ROOT: ${CI_PROJECT_DIR} 
  TF_STATE_NAME: default 

.terraform:format:
  stage: format
  script:
    - gitlab-terraform fmt
  allow_failure: true

.terraform:validate:
  stage: validate
  script:
    - gitlab-terraform validate
    - echo "$CI_TEMPLATE_REGISTRY_HOST , ${CI_PROJECT_DIR}, ${TF_ROOT}"
    # CI_PROJECT_DIR = /builds/arrowlevent/terraform-test       ;  TF_ROOT = /builds/arrowlevent/terraform-test
  allow_failure: true

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

terraform:format:
  extends: .terraform:format
  needs: []

terraform:validate:
  extends: .terraform:validate
  needs: []
```
  - Commit changes  --> Pipelines     ===>>> pipeline ve job'ları gözlemle


- 3. gitlab-ci.yml `Version_2.0`

BUILD AŞAMASINI EKLİYORSAN `terrraform plan` komutu çalışacağından   ***AWS Credentials*** BİLGİLERİNİ Gitlab Dasboard ÜZERİNDE ***Sensitive Variable*** OLARAK GİRMELİSİN. GitLab Dashboard ÜZERİNDE GİRDİYSEN BURADA YAZMANA GEREK YOK, YAZSAN DA HATA ALMAZSIN. AMA GENELDE KULLANILAN DEĞİŞKENLERİN TAMAMINI GÖRMEK DAHA İYİ OLACAĞINDAN BURADA BELİRTMELİSİN.

```yaml (gitlab-ci.yml)
stages:
  - format
  - validate
  - build

image:
  name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:v1.0.0"

variables:
  TF_ROOT: ${CI_PROJECT_DIR} 
  TF_STATE_NAME: default
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID             # ***AWS Credentials***   SENSITIVE
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY     # ***AWS Credentials***   SENSITIVE
  AWS_DEFAULT_REGION: "us-east-1"                   # NOT sensitive  

.terraform:format:
  stage: format
  script:
    - gitlab-terraform fmt
  allow_failure: true

.terraform:validate:
  stage: validate
  script:
    - gitlab-terraform validate
    - echo "$CI_TEMPLATE_REGISTRY_HOST , ${CI_PROJECT_DIR}, ${TF_ROOT}"
    # CI_PROJECT_DIR = /builds/arrowlevent/terraform-test       ;  TF_ROOT = /builds/arrowlevent/terraform-test
  allow_failure: true

.terraform:build:
  stage: build
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
    - ls          # README.md     main.tf     plan.cache      plan.json
  resource_group: ${TF_STATE_NAME}
  artifacts:
    # The next line, which disables public access to pipeline artifacts, may not be available everywhere.
    # See: https://docs.gitlab.com/ee/ci/yaml/#artifactspublic
    public: false
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json          

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

terraform:format:
  extends: .terraform:format
  needs: []

terraform:validate:
  extends: .terraform:validate
  needs: []

terraform:build:
  extends: .terraform:build
```
  - Commit changes  --> Pipelines     ===>>> pipeline ve job'ları gözlemle


- 4. gitlab-ci.yml `Version_3.0`  [https://docs.gitlab.com/ee/ci/yaml/index.html#rules]


```yaml (gitlab-ci.yml)
stages:
  - format
  - validate
  - build
  - deploy

image:
  name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:v1.0.0"

variables:
  TF_ROOT: ${CI_PROJECT_DIR}  # The relative path to the root directory of the Terraform project
  TF_STATE_NAME: default      # The name of the state file used by the GitLab Managed Terraform state backend
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID             # ***AWS Credentials***   SENSITIVE
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY     # ***AWS Credentials***   SENSITIVE
  AWS_DEFAULT_REGION: "us-east-1"                   # NOT sensitive

.terraform:format:
  stage: format
  script:
    - gitlab-terraform fmt
  allow_failure: true

.terraform:validate:
  stage: validate
  script:
    - gitlab-terraform validate

.terraform:build:
  stage: build
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  resource_group: ${TF_STATE_NAME}
  artifacts:
    public: false
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

.terraform:deploy:
  stage: deploy
  script:
    - gitlab-terraform apply
  resource_group: ${TF_STATE_NAME}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $TF_AUTO_DEPLOY == "true"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual      #  GitLab Dashboard üzerinde Pipeline içindeki job'lar çalışırken bu job'a geldiğinde manuel olarak ONAY ister

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

terraform:format:
  extends: .terraform:format
  needs: []


terraform:validate:
  extends: .terraform:validate
  needs: []

terraform:build:
  extends: .terraform:build

terraform:deploy:
  extends: .terraform:deploy
```
  - Commit changes  --> Pipelines     ===>>> pipeline ve job'ları gözlemle
  - AWS Management Console üzerinden s3 bucket oluştuğunu gözlemle


- 5. gitlab-ci.yml `Version_4.0`


```yaml (gitlab-ci.yml)
stages:
  - format
  - validate
  - build
  - deploy
  - cleanup

image:
  name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:v1.0.0"

variables:
  TF_ROOT: ${CI_PROJECT_DIR}  # The relative path to the root directory of the Terraform project
  TF_STATE_NAME: default      # The name of the state file used by the GitLab Managed Terraform state backend
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID             # ***AWS Credentials***   SENSITIVE
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY     # ***AWS Credentials***   SENSITIVE
  AWS_DEFAULT_REGION: "us-east-1"                   # NOT sensitive  

.format:
  stage: format
  script:
    - gitlab-terraform fmt
  allow_failure: true

.validate:
  stage: validate
  script:
    - gitlab-terraform validate

.build:
  stage: build
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  resource_group: ${TF_STATE_NAME}
  artifacts:
    public: false
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

.deploy:
  stage: deploy
  script:
    - gitlab-terraform apply
  resource_group: ${TF_STATE_NAME}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $TF_AUTO_DEPLOY == "true"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

.destroy:
  stage: cleanup
  script:
    - gitlab-terraform destroy
  resource_group: ${TF_STATE_NAME}
  when: manual    

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

format_job:
  extends: .format
  needs: []

validate_job:
  extends: .validate
  needs: []

build_job:
  extends: .build

deploy_job:
  extends: .deploy

destroy_job:
  extends: .destroy
```
  - Commit changes  --> Pipelines     ===>>> pipeline ve job'ları gözlemle
  - AWS Management Console üzerinden s3 bucket oluştuğunu/silindiğini gözlemle


- 6. gitlab-ci.yml `Version_5.0` Bu aşamaya kadar hidden job örnek olması için kullanıldı, aslında gerekli olmadığı için bu aşamada kullanmadım. Başka bir yerden hidden job referans göstermeden kendim yazacağıma bir örnek.


```yaml (gitlab-ci.yml)
stages:
  - format
  - validate
  - build
  - deploy
  - cleanup

default:  # HER 2 image DENENDİ ÇALIŞIYOR.
  image:
    # name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/releases/1.4:v1.0.0"
    name: "$CI_TEMPLATE_REGISTRY_HOST/gitlab-org/terraform-images/stable:latest"

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform/

variables:
  TF_ROOT: ${CI_PROJECT_DIR}  # The relative path to the root directory of the Terraform project
  TF_STATE_NAME: default      # The name of the state file used by the GitLab Managed Terraform state backend
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID             # ***AWS Credentials***   SENSITIVE
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY     # ***AWS Credentials***   SENSITIVE
  AWS_DEFAULT_REGION: "us-east-1"                   # NOT sensitive

format_job:
  stage: format
  script:
    - gitlab-terraform fmt
  allow_failure: true
  needs: []

validate_job:
  stage: validate
  script:
    - gitlab-terraform validate
  needs: ["format_job"]
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes:
        - "*.tf"

build:
  stage: build
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  resource_group: ${TF_STATE_NAME}
  artifacts:
    public: false
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

deploy:
  stage: deploy
  script:
    - gitlab-terraform apply
  resource_group: ${TF_STATE_NAME}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $TF_AUTO_DEPLOY == "true"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

destroy:
  stage: cleanup
  script:
    - gitlab-terraform destroy
  resource_group: ${TF_STATE_NAME}
  when: manual
```
  - Commit changes  --> Pipelines     ===>>> pipeline ve job'ları gözlemle
  - AWS Management Console üzerinden s3 bucket oluştuğunu/silindiğini gözlemle


# Resources

- https://docs.gitlab.com/ee/user/infrastructure/iac/
- https://docs.gitlab.com/ee/ci/yaml/index.html#extends
- https://docs.gitlab.com/ee/ci/jobs/#hide-jobs
- https://docs.gitlab.com/ee/ci/yaml/index.html#needs
- https://docs.gitlab.com/ee/ci/yaml/index.html#include
- https://docs.gitlab.com/ee/ci/yaml/index.html#includetemplate
- https://docs.gitlab.com/ee/user/infrastructure/iac/#use-a-terraform-template
- https://docs.gitlab.com/ee/ci/yaml/index.html#rules
- https://gitlab.com/gitlab-org/terraform-images
- https://gitlab.com/gitlab-org/gitlab/-/blob/83297544a8e7911a2062c789baca35cc990964ca/lib/gitlab/ci/templates/Terraform.latest.gitlab-ci.yml
- https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.latest.gitlab-ci.yml
- https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Terraform/Base.latest.gitlab-ci.yml
- https://docs.gitlab.com/ee/user/infrastructure/iac/gitlab_terraform_helpers.html
- https://docs.gitlab.com/ee/ci/yaml/#artifactspublic
- https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
- https://developer.hashicorp.com/terraform/language/settings/backends/http
- https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_template_recipes.html#run-a-custom-terraform-command-in-a-job
- https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_template_recipes.html#enable-a-terraform-destroy-job