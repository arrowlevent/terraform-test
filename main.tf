# creating AWS S3 Bucket using Terraform via GitLab CICD
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "terraform-bucket" {
  bucket = "arrow.terraform.gitlab"
}
